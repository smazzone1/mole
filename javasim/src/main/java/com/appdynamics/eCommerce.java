package com.appdynamics;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.UUID;


public class eCommerce
{
    public static void main(String[] args)
    {
        boolean running = true;
        int id = 1;
        UUID sessionId;

        ExecutorService executor = Executors.newFixedThreadPool(800);

        while (running) {
            sessionId = UUID.randomUUID();
            //System.out.println("Executing session Id: " + sessionId);
            eCommerceThread eCommerceThread = new eCommerceThread(id, sessionId);
            executor.execute(eCommerceThread);
            try
            {
                TimeUnit.MILLISECONDS.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            id++;
        }

        executor.shutdown();
        while (!executor.isTerminated()) {}
    }
}
