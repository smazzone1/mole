function Game()
{
    var maxBallsDropped = 100;
    var exceptionProb = 0.01;
    var colors = ["orange", "blue", "green", "purple", "red"];
    var balls;
    var ballsDropped;
    var ballsCaught;
    var exceptions;
    var soundOn = true;
    var ballCount = 0;

    // take this in a local variable to allow easy access to
    // this instace from within local functions
    var me = this;

    this.enter = function()
    {
        textSize(12);
        textAlign(LEFT);
        poseSetup();
        initGame();
    }

    var hasException= false;
    var showingExceptionDetails = false;
    var exceptionTime = 0;
    var exceptionBall;

    function showException(ball){
        hasException = true;
        exceptionBall = ball;
        showingExceptionDetails = false;
        exceptionTime=0;
    }

    this.draw = function()
    {
        if(hasException){
            if(!showingExceptionDetails){
                clear();
                image( this.sceneManager.bkImage, 0, 0);
                displayExceptionDetails(exceptionBall);
                showingExceptionDetails = true;
            }else{
                exceptionTime++;
                if(exceptionTime > 200){
                    showingExceptionDetails = false;
                    hasException = false;
                    hideExceptionDetails(exceptionBall);
                    exceptionTime=0;
                }
            }
        }

        clear();
        image( this.sceneManager.bkImage, 0, 0);
        displayBalls(balls);
        updateBalls(balls);

        addRandomBall(balls, exceptions);
        displayStats();
        poseDraw();
        
    }

    this.displayGlobalBalls = function()
    {
        displayBalls(balls);
    }

    this.getScore = function()
    {
        return ballsCaught;
    }

    function initGame()
    {
        balls = [];
        ballsDropped = 0;
        ballsCaught = 0;
        ballCount = 0;
        exceptions = getExceptions();
        var choice = Math.floor(random(0, exceptions.length));
        addBall(balls,exceptions[choice],choice);
    }

    function catchBall(ball)
    {
        ballsCaught++;
        if(ballsCaught > 20){
            ballsCaught = 0;
        }
        updateSound(laser);
        showException(ball);
        balls = balls.filter(function(value, index, arr) { return value != ball; });
    }

    function displayBalls(arBalls)
    {
        for(var i = 0; i < arBalls.length; i++)
        {
            displayBall( arBalls[i] );
        }
    }

    function updateSound(soundPlay){
        if(soundOn){
            soundPlay.play();
        }
    }

    function testBallHit(x, y, ballX, ballY, ballSize)
    {
        return dist(x, y, ballX, ballY) < ballSize / 2;
    }

    function containsHit(ball){
        if(hands){
            var result = false;
            for (let index = 0; index < hands.length; index++) {
                const pose = hands[index];
                if(testBallHit(pose.leftWrist.x+15,pose.leftWrist.y,ball.x,ball.y,ball.size) && !result){
                    result = true;
                    break;
                }
                if(testBallHit(pose.rightWrist.x+15,pose.rightWrist.y,ball.x,ball.y,ball.size) && !result){
                    result = true;
                    break;
                }
            }
            return result;
        }
    }

    function displayBall(ball)
    {
        fill(ball.blink ? Math.floor(Date.now() / 3000 * ball.stepSize) % 2 == 0 ? ball.color : "white" : ball.color);
        stroke("black");
        ellipse(ball.x, ball.y, ball.size);
        fill("white");
        textSize(14);
        textAlign(CENTER, CENTER);
        text(ball.ecount+"/min",ball.x-35,ball.y,ball.size);
        text(ball.ename,ball.x-30,ball.y+50,ball.size);
        
        if(containsHit(ball) || dist(mouseX, mouseY, ball.x, ball.y ) < ball.size / 2 )
        {
            noFill();
            ellipse(ball.x, ball.y, ball.size + 5);
            catchBall(ball);
        }else{
            if(!showingExceptionDetails){
                fill("white");
                textSize(16);
                text("Team Quarantine > Analytics Explorer",5,15,300);
            }
            fill("yellow");
            
            if(ballsCaught<5){
                textSize(36);
                text("Raise Your Palms and Cover Circle",5,windowHeight-500,700);
            }
            
            if(ballsCaught>5 && ballsCaught<10){
                textSize(26);
                image(team,0,windowHeight-400,400,300);
                text("Presented By: Team Quarantine : Richard P.,Shreyans P., Pavitra P., Bharath V., Ting L., Stefano M.",5,windowHeight-50,1200);
            }
            if(ballsCaught>10){
                ballsCaught=0;
                initGame();
            }
        }
    }

    function displayStats()
    {
        
    }

    var redColor = color("red");
    redColor.setAlpha(150);
    var greenColor = color("green");
    greenColor.setAlpha(150);

    function displayHealth(ballsDropped)
    {
        let redPerc = ballsDropped / maxBallsDropped;
        let greenPerc = 1 - redPerc;
        let redHeight = redPerc * windowHeight;
        let greenHeight = windowHeight - redHeight;
        
        stroke("black");
        fill(redColor);
        rect(0,0,60,redHeight);
        fill(greenColor);
        rect(0,redHeight,60,greenHeight);
        
        fill("yellow");   
        textSize(23);
        let healthPerc = round(greenPerc * 100);
        if (healthPerc > 0)
            text(healthPerc + "%", 30, windowHeight/2);
    }

    function updateBalls(arBalls)
    {
        for(var i = 0; i < arBalls.length; i++)
        {
            updateBall( arBalls[i] );
        }
    }


    function updateBall(ball)
    {
        ball.y += ball.stepSize;
        if (ball.y > height)
        {
            ball.y = 10;
            ballsDropped++;
            //updateSound(bangSmall);
        }
    }

    function addRandomBall(arBalls, exceptions)
    {
        var throwException = random(0,1);
        if (throwException < exceptionProb && ballCount < 20)
        {
            var choice = Math.floor(random(0, exceptions.length));
            addBall(balls,exceptions[choice],choice);
            ballCount++;
        }
    }

    function addBall(arBalls,exception,index)
    {
        var ecount = exception[2];
        var ball = { x : 0, y : 0, colorIndex : 0, color : "", size: 75, stepSize : 1, blink : false, eapp:exception[0], ename:exception[1], ecount:ecount, eindex:index};
        ball.x = random(40, width - 40);
        ball.colorIndex = Math.floor(random(0, colors.length));
        ball.color = colors[ball.colorIndex];
        arBalls.push(ball);
    }
}
