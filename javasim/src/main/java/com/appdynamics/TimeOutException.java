package com.appdynamics;

public class TimeOutException extends Exception{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public TimeOutException(String errorMessage) {
        super(errorMessage);
    }    

}
