
## Java Simulator

It simulates BT traffic on a controller

Please make sure to download the AppServerAgent first

## Compiling

cd javasim

mvn package

## Running
Use the run.sh script 
or 
java -javaagent:AppServerAgent/ver4.5.18.29239/javaagent.jar -cp target/javasim-1.0-SNAPSHOT.jar com.appdynamics.eCommerce