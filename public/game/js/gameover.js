function GameOver()
{
    var oGame;
    var me = this;

    this.setup = function()
    {
        // find a different scene using the SceneManager
        oGame = this.sceneManager.findScene( Game ).oScene;
    }

    this.draw = function()
    {
        // read the injected bkImage property
        image( this.sceneManager.bkImage, 0, 0);
        
        // invoke a method from a different scene
        oGame.displayGlobalBalls();

        fill("red");
        textSize( map( sin(frameCount * 0.1), 0, 1, 96, 128) );
        textAlign(CENTER);
        text("GAME OVER", width / 2, height / 2);



        textSize(24);
        text("Score: " + oGame.getScore(), width / 2, (height / 2) - 100);
        textSize(12);
        text("Press any key to restart game or wait 5 seconds...", width / 2, height - 20);
        setTimeout(restartGame,5000);
    }

    function restartGame(){
        me.sceneManager.showScene( Game );
    }

    this.keyPressed = function()
    {
        restartGame();
    }
}
