var bkImage;
var bangSmall;
var beat1;
var beat2;
var fire;

function preload()
{
    bkImage = loadImage("/game/img/nebula1.jpg");
    soundFormats('ogg', 'mp3');
    beat1 = loadSound('/game/sounds/beat1.mp3');
    beat2 = loadSound('/game/sounds/beat2.mp3');
    bangSmall = loadSound('/game/sounds/bangSmall.mp3');
    fire = loadSound('/game/sounds/fire.mp3');
}

function setup()
{
    createCanvas(windowWidth, windowHeight);
    var mgr = new SceneManager();
    mgr.bkImage = bkImage; // inject bkImage property
    mgr.wire();
    mgr.showScene( Intro );
}
