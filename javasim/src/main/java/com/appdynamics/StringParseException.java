package com.appdynamics;

public class StringParseException extends Exception {
    private static final long serialVersionUID = 1L;
    public StringParseException(String errorMessage) {
        super(errorMessage);
    }
}
