var bkImage;
var bangSmall;
var beat1;
var beat2;
var laser;
var fire;
var team;

function preload()
{
    bkImage = loadImage("/explore/img/nebula1.jpg");
    team = loadImage("/explore/img/team.png");
    soundFormats('ogg', 'mp3');
    laser = loadSound('/explore/sounds/laser.mp3');
}

function setup()
{
    createCanvas(windowWidth, windowHeight);
    var mgr = new SceneManager();
    mgr.bkImage = bkImage; // inject bkImage property
    mgr.wire();
    mgr.showScene( Intro );
}
