function Game()
{
    var maxBallsDropped = 100;
    var exceptionProb = 0.01;
    var colors = ["orange", "blue", "green", "purple", "red"];
    var balls;
    var ballsDropped;
    var ballsCaught;
    var exceptions;
    var soundOn = true;

    // take this in a local variable to allow easy access to
    // this instace from within local functions
    var me = this;

    this.enter = function()
    {
        textSize(12);
        textAlign(LEFT);
        poseSetup();
        initGame();
    }

    var hasException= false;
    var showingExceptionDetails = false;
    var exceptionTime = 0;
    var exceptionBall;

    function showException(ball){
        hasException = true;
        exceptionBall = ball;
        showingExceptionDetails = false;
        exceptionTime=0;
    }

    this.draw = function()
    {
        if(hasException){
            if(!showingExceptionDetails){
                clear();
                image( this.sceneManager.bkImage, 0, 0);
                displayExceptionDetails(exceptionBall);
                showingExceptionDetails = true;
            }else{
                exceptionTime++;
                if(exceptionTime > 300){
                    showingExceptionDetails = false;
                    hasException = false;
                    hideExceptionDetails(exceptionBall);
                    exceptionTime=0;
                }
            }
        }

        clear();
        image( this.sceneManager.bkImage, 0, 0);
        displayBalls(balls);
        updateBalls(balls);
        addRandomBall(balls, exceptions);
    
        displayStats();
        poseDraw();
    }

    this.mousePressed = function()
    {
        var ball = findHitBall(balls);
        if ( ball != null )
        {
            catchBall(ball);
        }
    }

    this.displayGlobalBalls = function()
    {
        displayBalls(balls);
    }

    this.getScore = function()
    {
        return ballsCaught;
    }

    function initGame()
    {
        balls = [];
        ballsDropped = 0;
        ballsCaught = 0;
        exceptions = getExceptions();
        var choice = Math.floor(random(0, exceptions.length));
        addBall(balls,exceptions[choice],choice);
    }

    function catchBall(ball)
    {
        if ( ballsDropped < maxBallsDropped )
        {
            ballsCaught++;
            balls = balls.filter(function(value, index, arr) { return value != ball; });
            updateSound(fire);
        }
    }

    function displayBalls(arBalls)
    {
        for(var i = 0; i < arBalls.length; i++)
        {
            displayBall( arBalls[i] );
        }
    }

    function updateSound(soundPlay){
        if(soundOn){
            soundPlay.play();
        }
    }

    function testBallHit(x, y, ballX, ballY, ballSize)
    {
        return dist(x, y, ballX, ballY) < ballSize / 2;
    }

    function displayBall(ball)
    {
        fill(ball.blink ? Math.floor(Date.now() / 3000 * ball.stepSize) % 2 == 0 ? ball.color : "white" : ball.color);
        stroke("black");
        ellipse(ball.x, ball.y, ball.size);
        fill("white");
        textSize(14);
        textAlign(CENTER, CENTER);
        text(ball.ecount+"/min",ball.x-35,ball.y,ball.size);
        text(ball.ename,ball.x-30,ball.y+50,ball.size);
        

        if (testBallHit(leftPaddle.x - paddleWidth / 2, leftPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(leftPaddle.x + paddleWidth / 2, leftPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(leftPaddle.x - paddleWidth / 2, leftPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(leftPaddle.x + paddleWidth / 2, leftPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(rightPaddle.x - paddleWidth / 2, rightPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(rightPaddle.x + paddleWidth / 2, rightPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(rightPaddle.x - paddleWidth / 2, rightPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
            testBallHit(rightPaddle.x + paddleWidth / 2, rightPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
            dist(mouseX, mouseY, ball.x, ball.y ) < ball.size / 2)
        {
            noFill();
            ellipse(ball.x, ball.y, ball.size + 5);

            if ( me.sceneArgs == '1' )
            {
                catchBall(ball);
            }
            showException(ball);
        }
    }

    function displayStats()
    {
        fill("yellow");
        textAlign(RIGHT);
        textSize(24);
        text("Caught: " + ballsCaught, windowWidth - 10, 20);
        text("Missed: " + ballsDropped, windowWidth - 10, 50);
        textSize(12);
        textAlign(CENTER);
        displayHealth(ballsDropped);
        
        if (frameCount % 20 == 0)
            updateSound(beat1);
    }

    var redColor = color("red");
    redColor.setAlpha(150);
    var greenColor = color("green");
    greenColor.setAlpha(150);

    function displayHealth(ballsDropped)
    {
        let redPerc = ballsDropped / maxBallsDropped;
        let greenPerc = 1 - redPerc;
        let redHeight = redPerc * windowHeight;
        let greenHeight = windowHeight - redHeight;
        
        stroke("black");
        fill(redColor);
        rect(0,0,60,redHeight);
        fill(greenColor);
        rect(0,redHeight,60,greenHeight);
        
        fill("yellow");   
        textSize(23);
        let healthPerc = round(greenPerc * 100);
        if (healthPerc > 0)
            text(healthPerc + "%", 30, windowHeight/2);
    }

    function verticalText(input, x, y) {
        var output = "";  // create an empty string
       
        for (var i = 0; i < input.length; i += 1) {
          output += input.charAt(i) + "\n"; // add each character with a line break in between
        }
        
        push(); // use push and pop to restore style (in this case the change in textAlign) after displaing the text 
        textAlign(CENTER, TOP); // center the characters horizontaly with CENTER and display them under the provided y with TOP
        fill("yellow");
        text(output, x, y); // display the text
        pop();
      }

    function findHitBall(arBalls)
    {
        for(var i = 0; i < arBalls.length; i++)
        {
            var ball = arBalls[i];
            
            if (testBallHit(leftPaddle.x - paddleWidth / 2, leftPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(leftPaddle.x + paddleWidth / 2, leftPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(leftPaddle.x - paddleWidth / 2, leftPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(leftPaddle.x + paddleWidth / 2, leftPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(rightPaddle.x - paddleWidth / 2, rightPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(rightPaddle.x + paddleWidth / 2, rightPaddle.y - paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(rightPaddle.x - paddleWidth / 2, rightPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size) ||
                testBallHit(rightPaddle.x + paddleWidth / 2, rightPaddle.y + paddleHeight / 2, ball.x, ball.y, ball.size))
            {
                return ball;
            }

            var mouseDist = dist(mouseX, mouseY, ball.x, ball.y);
            if ( mouseDist < ball.size / 2)
            {
                return ball;
            }
        }

        return null;
    }


    function updateBalls(arBalls)
    {
        for(var i = 0; i < arBalls.length; i++)
        {
            updateBall( arBalls[i] );
        }
    }


    function updateBall(ball)
    {
        ball.y += ball.stepSize;
        if (ball.y > height)
        {
            ball.y = 10;
            ballsDropped++;
            updateSound(bangSmall);
            showException(ball);
        }
    }

    function addRandomBall(arBalls, exceptions)
    {
        var throwException = random(0,1);
        if (throwException < exceptionProb)
        {
            var choice = Math.floor(random(0, exceptions.length));
            addBall(balls,exceptions[choice],choice);
        }
    }

    function addBall(arBalls,exception,index)
    {
        var ecount = exception[2];
        var ball = { x : 0, y : 0, colorIndex : 0, color : "", size: 75, stepSize : 1, blink : false, eapp:exception[0], ename:exception[1], ecount:ecount, eindex:index};
        ball.x = random(40, width - 40);
        ball.colorIndex = Math.floor(random(0, colors.length));
        ball.color = colors[ball.colorIndex];
        arBalls.push(ball);
    }
}
