package com.appdynamics;

public class SQLException extends Exception {
    private static final long serialVersionUID = 1L;
    public SQLException(String errorMessage) {
        super(errorMessage);
    }
}