var getExceptions = function(){
    return [
        ["App1","NullPointerException",15],
        ["App2","NumberFormatException",50],
        ["App3","CoreFormatException",150],
        ["App2","InvalidNumberException",50],
        ["App2","AuthenticationException",50],
        ["App2","GeoException",140],
        ["App2","MathException",30],
        ["App2","SecurityException",90]
    ];
}

var showDetails = function(exception){
    $("#details").show();
    var boxChartOptions = {
        max:10,
        size:{height:100,width:800},
        axis: {
            x: { show: true, "type": "timeseries",tick:{fit:false, culling:{max:3}} },
            y: { show: true, tick:{fit:false, culling:{max:3}} }
        }
    }
    new BoxChartComponent({
        title:exception.ename,
        targetId:"details",
        actionClick: "window.location.href='/views/examples/boxcomponent.html'",
        icon:"fas fa-bug",
        options:boxChartOptions
    }).draw();
}

let video;
let poseNet;
let leftFeature;
let rightFeature;
let leftPose;
let rightPose;
let skeleton;
const videoWidth = window.innerWidth;
const videoHeight = window.innerHeight;

var leftPaddle = {init:false};
var rightPaddle = {init:false};

function modelLoaded() {
    
}

function poseSetup() {
  video = createCapture(VIDEO);
  video.size(videoWidth,videoHeight);
  video.id("vid");
  video.hide();

  var options = {
    imageScaleFactor: 0.5,
    outputStride: 16,
    flipHorizontal: true,
    minConfidence: 0.9,
    maxPoseDetections: 5,
    scoreThreshold: 0.9,
    nmsRadius: 20,
    detectionType: 'multiple',
    multiplier: 1.0,
  }

  poseNet = ml5.poseNet(video, options,modelLoaded);
  poseNet.on('pose', gotPoses);
}

function gotPoses(poses) {
  allPoses = poses;
  var leftMaxConfidence = 0.0;
  var rightMaxConfidence = 0.0;
  for (var i = 0; i < poses.length; ++i)
  {
    var pose = poses[i].pose;
    var leftFeatures = [pose.nose, pose.leftEye, pose.leftEar, pose.leftShoulder, pose.leftElbow, pose.leftWrist, pose.leftHip, pose.leftKnee, pose.leftAnkle];
    for (var j = 0; j < leftFeatures.length; ++j)
    {
      if (leftFeatures[j].confidence > leftMaxConfidence)
      {
        leftPose = pose;
        leftFeature = leftFeatures[j];
        leftMaxConfidence = leftFeatures[j].confidence;
      }
    }

    var rightFeatures = [pose.nose, pose.rightEye, pose.rightEar, pose.rightShoulder, pose.rightElbow, pose.rightWrist, pose.rightHip, pose.rightKnee, pose.rightAnkle];
    for (var j = 0; j < rightFeatures.length; ++j)
    {
      if (rightFeatures[j].confidence > rightMaxConfidence)
      {
        rightPose = pose;
        rightFeature = rightFeatures[j];
        rightMaxConfidence = rightFeatures[j].confidence;
      }
    }
  }
}

var paddleWidth = 100;
var paddleHeight = 20;
var sensitivity = 27.0;
var beta1 = 0.9;
var beta2 = 0.999;
var epsilon = 1e-10;

function updatePaddle(paddle, x, y, w, h)
{
    var d = dist(paddle.x, paddle.y, x, y);
    if (isNaN(paddle.x))
    {
      paddle.x = x;
      paddle.horizMomentum = 0;
      paddle.horizEnergy = 0;
    }
    else
    {
      var update = x - paddle.x;
      paddle.horizMomentum = beta1 * paddle.horizMomentum + (1 - beta1) * update;
      paddle.horizEnergy = beta2 * paddle.horizEnergy + (1 - beta2) * update * update;
      paddle.x = paddle.x + sensitivity * paddle.horizMomentum / (Math.sqrt(paddle.horizEnergy) + epsilon);
    }
    if (isNaN(paddle.y))
    {
      paddle.y = y;
      paddle.vertMomentum = 0;
      paddle.vertEnergy = 0;
    }
    else
    {
      var update = y - paddle.y;
      paddle.vertMomentum = beta1 * paddle.vertMomentum + (1 - beta1) * update;
      paddle.vertEnergy = beta2 * paddle.vertEnergy + (1 - beta2) * update * update;
      paddle.y = paddle.y + sensitivity * paddle.vertMomentum / (Math.sqrt(paddle.vertEnergy) + epsilon);
    }
    paddle.w = w;
    paddle.h = h;
}

function poseDraw()
{
  fill(169,169,169);
    
  if (leftFeature)
  {
    updatePaddle(leftPaddle, leftFeature.x, leftFeature.y, paddleWidth, paddleHeight);
    stroke("grey");
    rect(leftPaddle.x, leftPaddle.y, leftPaddle.w, leftPaddle.h);
  }

  if (rightFeature)
  {
    updatePaddle(rightPaddle, rightFeature.x, rightFeature.y, paddleWidth, paddleHeight);
    stroke("grey");
    rect(rightPaddle.x, rightPaddle.y, rightPaddle.w, rightPaddle.h);
  }

  drawPerson(rightPose);
  drawPerson(leftPose);
}	

var validPartsToDraw = ['nose','leftShoulder','rightShoulder','leftElbow','rightElbow','leftWrist','rightWrist','rightKnee','rightAnkle','leftKnee','leftAnkle'];
function drawPart(part){
  return validPartsToDraw.indexOf(part) >= 0;
}

function drawPerson(pose){
  if (pose) {
    for(var i = 0; i < pose.keypoints.length; i++) {
      var keypoint = pose.keypoints[i];
      // filter out keypoints that have a low confidence
      if (drawPart(keypoint.part)) {
        // for wrists, make the part red
        if (keypoint.part == 'leftWrist'){
          fill("green");
          ellipse(keypoint.position.x, keypoint.position.y, 20, 20);
        } else if (keypoint.part == 'rightWrist'){
          fill("green");
          ellipse(keypoint.position.x, keypoint.position.y, 20, 20);
        }else{
          if(keypoint.part == 'nose'){
            fill("red");
            ellipse(keypoint.position.x, keypoint.position.y, 20, 20);
          }else{
            fill("white");
            ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
          }
        }
      }
    }
    stroke("green");
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.rightShoulder.x, pose.rightShoulder.y);

    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftElbow.x, pose.leftElbow.y);
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftHip.x, pose.leftHip.y);
    line(pose.leftHip.x, pose.leftHip.y,pose.leftKnee.x,pose.leftKnee.y);
    line(pose.leftKnee.x, pose.leftKnee.y,pose.leftAnkle.x,pose.leftAnkle.y);
    line(pose.leftElbow.x, pose.leftElbow.y, pose.leftWrist.x, pose.leftWrist.y);
    
    line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightElbow.x, pose.rightElbow.y);
    line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightHip.x, pose.rightHip.y);
    line(pose.rightHip.x, pose.rightHip.y,pose.rightKnee.x,pose.rightKnee.y);
    line(pose.rightKnee.x, pose.rightKnee.y,pose.rightAnkle.x,pose.rightAnkle.y);
    line(pose.rightElbow.x, pose.rightElbow.y, pose.rightWrist.x, pose.rightWrist.y);
    line(pose.rightHip.x, pose.rightHip.y, pose.leftHip.x, pose.leftHip.y);
  }
}