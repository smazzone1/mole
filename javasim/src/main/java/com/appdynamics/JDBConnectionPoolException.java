package com.appdynamics;

public class JDBConnectionPoolException extends Exception{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public JDBConnectionPoolException(String errorMessage) {
        super(errorMessage);
    }
}
