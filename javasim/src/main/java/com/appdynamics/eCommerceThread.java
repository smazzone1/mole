package com.appdynamics;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;



public class eCommerceThread implements Runnable {
    private static final Logger log = Logger.getLogger(eCommerceThread.class.getName());
    private String sessionID;
    private String orderID;
    private int current;

    private enum ErrorType {
        AuthenticationException, TimeOutException, SQLException, NumberFormatException,
        InvalidUserException, NullPointerException, IllegalAccessException, SecurityException, StringParseException,
        JDBCConnectionPoolException
    };

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt(max - min + 1) + min;
        return randomNum;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public void setSessionID(UUID id) {
        this.sessionID = ("Session" + id.toString());
    }

    public String getOrderID() {
        return this.orderID;
    }

    public void setOrderID(int id) {
        this.orderID = ("Order" + id);
    }

    public int getCurrent() {
        return this.current;
    }

    public void setCurrent(int id) {
        this.current = id;
    }

    eCommerceThread(int id, UUID sessionId) {
        setSessionID(sessionId);
        setCurrent(id);
    }

    public void run() {
        RandomCustomer randomCustomer = new RandomCustomer();
        String customerType = randomCustomer.customerType;
        String customerEmail = randomCustomer.customerEmail;
        String productType = randomCustomer.productType;
        String productCategory = randomCustomer.productCategory;
        String productName = randomCustomer.productName;
        Integer productPrice = randomCustomer.productPrice;
        boolean everyTenOrders;
        // for onprem set to 10
        // for cloud set to 5
        if (getCurrent() % 5 == 0) {
            everyTenOrders = true;
        } else {
            everyTenOrders = false;
        }

        boolean nextStep = homePage();
        int price;
        if ((nextStep) || (everyTenOrders)) {
            nextStep = logIn();

            if ((nextStep) || (everyTenOrders)) {
                nextStep = addToCart();

                if ((nextStep) || (everyTenOrders)) {
                    if (everyTenOrders)
                        setOrderID(getCurrent());
                    price = checkOut(customerType, customerEmail, productType, productCategory, productName,
                            productPrice);
                }
            }
        }
    }

    public boolean homePage() {
        processTransaction();

        int rand = randInt(1, 100);
        if (rand <= 50) {
            normalTransaction();
        } else if ((rand > 50) && (rand <= 60)) {
            slowTransaction();
        } else if (rand > 60) {
            errorTransactionCustom(getRandomException());
        }

        // for onprem set to 85
        // for cloud set to 98
        rand = randInt(1, 100);
        if (rand <= 60) {
            return true;
        }
        return false;
    }

    public boolean logIn() {
        processTransaction();

        int rand = randInt(1, 100);
        if (rand <= 50) {
            normalTransaction();
        } else if ((rand > 50) && (rand <= 60)) {
            slowTransaction();
        } else if (rand > 60) {
            errorTransactionCustom(getRandomException());
        }

        // for onprem set to 75
        // for cloud set to 90
        rand = randInt(1, 100);
        if (rand <= 60) {
            return true;
        }
        return false;
    }

    public boolean addToCart() {
        processTransaction();

        int rand = randInt(1, 100);
        // for onprem set to 50
        // for cloud set to 95
        if (rand <= 50) {
            normalTransaction();
            // for onprem set to 50 and 70
            // for cloud set to 95 and 98
        } else if ((rand > 50) && (rand <= 60)) {
            slowTransaction();
            // for onprem set to 70
            // for cloud set to 98
        } else if (rand > 60) {
            //errorTransaction("Random Error!");
            errorTransactionCustom(getRandomException());
        }

        rand = randInt(1, 100);
        // for onprem set to 30
        // for cloud set to 98
        if (rand <= 60) {
            return true;
        }
        return false;
    }

    public int checkOut(String customerType, String customerEmail, String productType, String productCategory,
            String productName, Integer itemPrice) {
        processTransaction();

        int rand = randInt(1, 100);
        if (rand <= 95) {
            normalTransaction();
        } else if ((rand > 95) && (rand <= 98)) {
            slowTransaction();
        } else if (rand > 98) {
            errorTransactionCustom(getRandomException());
        }

        return itemPrice.intValue();
    }

    public void processTransaction() {
        try {
            // for onprem set to 100 and 200
            // for cloud set to 50 and 100
            TimeUnit.MILLISECONDS.sleep(randInt(50, 100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void normalTransaction() {
        // for onprem set to 800 and 1000
        // for cloud set to 500 and 800
        try {
            TimeUnit.MILLISECONDS.sleep(randInt(500, 800));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void slowTransaction() {
        try {
            // for on prem set to 3000 and 4000
            // for cloud set to 1500 and 2000
            TimeUnit.MILLISECONDS.sleep(randInt(1500, 2000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void errorTransaction(String errorMsg) {
        try {
            throw new RuntimeException(errorMsg);
        } catch (RuntimeException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        }
    }

    private ErrorType getRandomException() {
        int pick = new Random().nextInt(ErrorType.values().length);
        return ErrorType.values()[pick];
    }

    private void errorTransactionCustom(ErrorType etype) {
        try {
            switch (etype) {
            case AuthenticationException:
                throw new AuthenticationException("Error Invalid Authentication");
            case IllegalAccessException:
                throw new IllegalAccessException("Error Illegal Access");
            case InvalidUserException:
                throw new InvalidUserException("Error Invalid User");
            case JDBCConnectionPoolException:
                throw new JDBConnectionPoolException("JDBC Connection Refused");
            case TimeOutException:
                throw new TimeOutException("Timeout");
            case NullPointerException:
                throw new NullPointerException("Ops something bad has happened");
            case NumberFormatException:
                throw new NumberFormatException("Input data malformed");
            case SQLException:
                throw new SQLException("SQL malformed");
            case SecurityException:
                throw new SecurityException("Security Error");
            case StringParseException:
                throw new StringParseException("String malformed");
            default:
          }
        } catch (AuthenticationException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (IllegalAccessException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (InvalidUserException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (JDBConnectionPoolException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (TimeOutException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (RuntimeException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (SQLException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        } catch (StringParseException ex) {
            log.log(Level.SEVERE, ex.toString(), ex);
        }
    }

}
